/*
Navicat MySQL Data Transfer

Source Server         : 127.0.0.1
Source Server Version : 50725
Source Host           : 127.0.0.1:3306
Source Database       : db_xkube

Target Server Type    : MYSQL
Target Server Version : 50725
File Encoding         : 65001

Date: 2024-06-25 16:55:18
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for group
-- ----------------------------
DROP TABLE IF EXISTS `group`;
CREATE TABLE `group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `title` varchar(100) NOT NULL DEFAULT '',
  `status` int(11) NOT NULL DEFAULT '2',
  `sort` int(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of group
-- ----------------------------
INSERT INTO `group` VALUES ('1', 'rbac', '权限管理', '2', '1');
INSERT INTO `group` VALUES ('3', 'xkube', 'k8s管理', '2', '2');
INSERT INTO `group` VALUES ('4', 'wiki', '文档中心', '2', '3');
INSERT INTO `group` VALUES ('5', 'cicd', 'CICD', '2', '4');

-- ----------------------------
-- Table structure for node
-- ----------------------------
DROP TABLE IF EXISTS `node`;
CREATE TABLE `node` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `level` int(11) NOT NULL DEFAULT '1',
  `pid` bigint(20) NOT NULL DEFAULT '0',
  `icons` varchar(200) DEFAULT NULL,
  `sorts` int(11) DEFAULT '1',
  `remark` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  `group_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=246 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of node
-- ----------------------------
INSERT INTO `node` VALUES ('1', '权限管理', 'rbac', '1', '0', 'layui-icon-auz', '7', '', '2', '1');
INSERT INTO `node` VALUES ('2', '目录结构', 'node', '2', '1', null, '4', '', '2', '1');
INSERT INTO `node` VALUES ('3', '列表', 'List', '3', '2', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('4', '增改', 'AddAndEdit', '3', '2', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('5', '删除', 'Delete', '3', '2', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('6', '管理员', 'user', '2', '1', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('7', '管理员列表', 'List', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('8', '添加管理员', 'Add', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('9', '更新管理员', 'Update', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('10', '删除管理员', 'Delete', '3', '6', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('11', '目录分组', 'group', '2', '1', null, '3', '', '2', '1');
INSERT INTO `node` VALUES ('12', '分组列表', 'List', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('13', '添加分组', 'Add', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('14', '修改分组', 'Update', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('15', '删除分组', 'Delete', '3', '11', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('16', '角色管理', 'role', '2', '1', null, '2', '', '2', '1');
INSERT INTO `node` VALUES ('17', '角色列表', 'List', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('18', '添加编辑角色', 'AddAndEdit', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('19', '删除角色', 'Delete', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('20', 'get roles', 'Getlist', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('21', 'show access', 'AccessToNode', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('22', 'add accsee', 'AddAccess', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('23', 'show role to userlist', 'RoleToUserList', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('24', 'add role to user', 'AddRoleToUser', '3', '16', null, '1', '', '2', '1');
INSERT INTO `node` VALUES ('27', '获取目录列表', 'Getlist', '3', '2', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('28', '获取父节点id', 'GetPid', '3', '2', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('29', 'RoleToNodeList', 'RoleToNodeList', '3', '16', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('30', 'DelRoleToUser', 'DelRoleToUser', '3', '16', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('31', 'DelRoleToNode', 'DelRoleToNode', '3', '16', null, '1', 'add by kang', '2', '1');
INSERT INTO `node` VALUES ('34', 'k8s管理', 'xkube', '1', '0', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('35', '无状态', 'deploy', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('36', '列表', 'v1/List', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('37', '集群管理', 'cluster', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('38', '集群列表', 'v1/List', '3', '37', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('39', '有状态', 'sts', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('40', '守护进程', 'ds', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('41', '任务', 'job', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('42', '定时任务', 'cronjob', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('43', '容器组', 'pod', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('44', '自定义资源', 'cdr', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('45', '自动伸缩', 'hpa', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('46', '事件中心', 'event', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('47', 'yam操作', 'apply', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('48', '命名空间', 'ns', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('49', '节点管理', 'node', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('50', '服务管理', 'svc', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('51', '路由', 'ing', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('52', 'configmap', 'cm', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('53', 'secrets', 'secret', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('54', '存储声明', 'pvc', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('55', '存储卷', 'pv', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('56', '存储类', 'storageclass', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('57', 'clusterrolebinding', 'clusterrolebinding', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('58', 'clusterroles', 'clusterroles', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('59', 'serviceaccounts', 'serviceaccounts', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('60', 'roles', 'roles', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('61', 'rolebinding', 'rolebinding', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('62', 'metrics', 'metrics', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('65', '文档中心', 'wiki', '1', '0', '', '0', '', '2', '4');
INSERT INTO `node` VALUES ('66', '列表', 'v1/List', '2', '65', '', '0', '', '2', '4');
INSERT INTO `node` VALUES ('67', '添加', 'v1/Add', '2', '65', '', '0', '', '2', '4');
INSERT INTO `node` VALUES ('68', '更新', 'v1/Update', '2', '65', '', '0', '', '2', '4');
INSERT INTO `node` VALUES ('69', '删除', 'v1/Del', '2', '65', '', '0', '', '2', '4');
INSERT INTO `node` VALUES ('70', '读取', 'v1/Read', '2', '65', '', '0', '', '2', '4');
INSERT INTO `node` VALUES ('71', '加密判断', 'v1/ReadEncry', '2', '65', '', '0', '', '2', '4');
INSERT INTO `node` VALUES ('72', 'GetridByuid', 'GetridByuid', '3', '16', '', '0', '', '2', '1');
INSERT INTO `node` VALUES ('73', '集群授权', 'cluster', '2', '1', '', '0', '', '2', '1');
INSERT INTO `node` VALUES ('74', '列表', 'List', '3', '73', '', '0', '', '2', '1');
INSERT INTO `node` VALUES ('75', '添加', 'Add', '3', '73', '', '0', '', '2', '1');
INSERT INTO `node` VALUES ('76', '删除', 'Delete', '3', '73', '', '0', '', '2', '1');
INSERT INTO `node` VALUES ('77', '添加', 'v1/Add', '3', '37', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('78', '更新', 'v1/Update', '3', '37', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('79', '编辑', 'v1/Edit', '3', '37', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('80', '删除', 'v1/Del', '3', '37', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('81', '详情', 'v1/Detail', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('82', '创建', 'v1/Create', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('83', '更新', 'v1/Modify', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('84', 'yaml更新', 'v1/ModifyByYaml', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('85', '删除', 'v1/Del', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('86', 'yaml查看', 'v1/Yaml', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('87', 'ReplicasetYaml', 'v1/ReplicasetYaml', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('88', 'Replicaset列表', 'v1/ReplicasetList', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('89', '回滚', 'v1/RollBack', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('90', '重启', 'v1/Restart', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('91', '标签', 'v1/Labels', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('92', '镜像', 'v1/Image', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('93', '列表', 'v1/List', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('94', '详情', 'v1/Detail', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('95', '创建', 'v1/Create', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('96', '更新', 'v1/Modify', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('97', '删除', 'v1/Del', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('98', 'yaml查看', 'v1/Yaml', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('99', '回滚', 'v1/RollBack', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('100', '重启', 'v1/Restart', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('101', '列表', 'v1/List', '3', '40', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('102', '详情', 'v1/Detail', '3', '40', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('103', '创建', 'v1/Create', '3', '40', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('104', '更新', 'v1/Modify', '3', '40', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('105', '删除', 'v1/Del', '3', '40', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('106', 'yaml查看', 'v1/Yaml', '3', '40', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('107', 'yaml更新', 'v1/ModifyByYaml', '3', '40', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('108', '列表', 'v1/List', '3', '41', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('109', '详情', 'v1/Detail', '3', '41', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('110', '创建', 'v1/Create', '3', '41', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('111', '更新', 'v1/Modify', '3', '41', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('112', '删除', 'v1/Del', '3', '41', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('113', 'yaml查看', 'v1/Yaml', '3', '41', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('114', '日志', 'v1/Log', '3', '41', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('115', '列表', 'v1/List', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('116', '详情', 'v1/Detail', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('117', '创建', 'v1/Create', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('118', '更新', 'v1/Modify', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('119', 'yaml更新', 'v1/ModifyByYaml', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('120', '删除', 'v1/Del', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('121', '查看yaml', 'v1/Yaml', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('122', '标签', 'v1/Labels', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('123', '列表', 'v1/List', '3', '50', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('124', '详情', 'v1/Detail', '3', '50', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('125', '创建', 'v1/Create', '3', '50', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('126', 'yaml更新', 'v1/ModifyByYaml', '3', '50', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('127', '删除', 'v1/Del', '3', '50', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('128', 'yaml查看', 'v1/Yaml', '3', '50', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('129', '列表', 'v1/List', '3', '52', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('130', '详情', 'v1/Detail', '3', '52', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('131', '创建', 'v1/Create', '3', '52', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('132', 'yaml更新', 'v1/ModifyByYaml', '3', '52', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('133', '删除', 'v1/Del', '3', '52', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('134', 'yaml查看', 'v1/Yaml', '3', '52', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('135', 'yaml查看', 'v1/Yaml', '3', '53', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('136', '列表', 'v1/List', '3', '53', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('137', '详情', 'v1/Detail', '3', '53', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('138', '创建', 'v1/Create', '3', '53', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('139', 'yaml更新', 'v1/ModifyByYaml', '3', '53', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('140', '删除', 'v1/Del', '3', '53', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('141', '列表', 'v1/List', '3', '51', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('142', '详情', 'v1/Detail', '3', '51', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('143', '创建', 'v1/Create', '3', '51', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('144', 'yaml更新', 'v1/ModifyByYaml', '3', '51', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('145', '删除', 'v1/Del', '3', '51', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('146', 'yaml查看', 'v1/Yaml', '3', '51', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('147', '列表', 'v1/List', '3', '43', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('148', 'container列表', 'v1/ContainerList', '3', '43', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('149', '详情', 'v1/Detail', '3', '43', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('150', '日志', 'v1/Log', '3', '43', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('151', '删除', 'v1/Del', '3', '43', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('152', 'yaml查看', 'v1/Yaml', '3', '43', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('153', '列表', 'v1/List', '3', '45', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('154', 'yaml查看', 'v1/Yaml', '3', '45', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('155', '创建', 'v1/Create', '3', '45', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('156', '删除', 'v1/Del', '3', '45', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('157', 'PodList', 'v1/PodList', '3', '62', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('158', 'PodUsage', 'PodUsage', '3', '62', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('159', 'NodeUsage', 'NodeUsage', '3', '62', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('160', 'NodeList', 'v1/NodeList', '3', '62', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('161', '列表', 'v1/List', '3', '49', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('162', '节点池列表', 'v1/PoolList', '3', '49', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('163', '详情', 'v1/Detail', '3', '49', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('164', 'yaml查看', 'v1/Yaml', '3', '49', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('165', '不调度', 'v1/Unschedulable', '3', '49', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('166', '排水', 'v1/Drain', '3', '49', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('167', '删除', 'v1/Del', '3', '49', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('168', '列表', 'v1/List', '3', '48', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('169', '详情', 'v1/Detail', '3', '48', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('170', '创建', 'v1/Create', '3', '48', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('171', '删除', 'v1/Del', '3', '48', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('172', 'yaml更新', 'v1/ModifyByYaml', '3', '48', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('173', 'yaml查看', 'v1/Yaml', '3', '48', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('174', '资源限制', 'v1/LimitRange', '3', '48', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('175', 'yaml创建', 'v1/CreateByYaml', '3', '47', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('176', 'ApplyYaml', 'v1/ApplyYaml', '3', '47', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('177', '列表', 'v1/List', '3', '44', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('178', 'yaml查看', 'v1/Yaml', '3', '44', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('179', '删除', 'v1/Del', '3', '44', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('180', '列表', 'v1/List', '3', '46', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('181', '应用集', 'appname', '2', '34', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('182', '列表', 'v1/List', '3', '56', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('183', 'yaml查看', 'v1/Yaml', '3', '56', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('184', '详情', 'v1/Detail', '3', '56', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('185', '列表', 'v1/List', '3', '55', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('186', '详情', 'v1/Detail', '3', '55', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('187', 'yaml查看', 'v1/Yaml', '3', '55', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('188', '列表', 'v1/List', '3', '54', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('189', 'yaml查看', 'v1/Yaml', '3', '54', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('190', '详情', 'v1/Detail', '3', '54', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('191', '列表', 'v1/List', '3', '57', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('192', '列表', 'v1/List', '3', '58', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('193', '列表', 'v1/List', '3', '59', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('194', '列表', 'v1/List', '3', '60', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('195', '列表', 'v1/List', '3', '61', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('196', 'yaml查看', 'v1/Yaml', '3', '57', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('197', 'yaml查看', 'v1/Yaml', '3', '58', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('198', 'yaml查看', 'v1/Yaml', '3', '59', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('199', 'yaml查看', 'v1/Yaml', '3', '60', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('200', 'yaml查看', 'v1/Yaml', '3', '61', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('201', '首页统计', 'v1/Count', '3', '37', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('202', 'beta列表', 'beta1/List', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('203', 'beta详情', 'beta1/Detail', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('204', 'beta创建', 'beta1/Create', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('205', 'beta更新', 'beta1/Modify', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('206', 'beta yaml更新', 'beta1/ModifyByYaml', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('207', 'beta删除', 'beta1/Del', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('208', 'beta查看yaml', 'beta1/Yaml', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('209', 'beta标签', 'beta1/Labels', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('210', '应用集列表', 'v1/List', '3', '181', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('211', '添加', 'v1/Add', '3', '181', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('212', '编辑', 'v1/Edit', '3', '181', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('213', '更新', 'v1/Update', '3', '181', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('214', '删除', 'v1/Del', '3', '181', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('215', 'CICD', 'cicd', '1', '0', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('216', '列表', 'v1/List', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('217', '添加', 'v1/Add', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('218', '更新', 'v1/Update', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('219', '编辑', 'v1/Edit', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('220', '删除', 'v1/Del', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('221', 'ak列表', 'v1/AkList', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('222', 'ak添加', 'v1/AkAdd', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('223', 'ak删除', 'v1/AkDel', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('224', 'cicd信息', 'v1/GetCicdInfo', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('225', '流水线信息', 'v1/GetPipelines', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('226', '应用名列表', 'v1/ListAppname', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('227', '更改cicd状态', 'v1/PostStatus', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('228', '阿里云id列表', 'v1/GetAliyunIdList', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('229', '运行流水线', 'pipeline/Start', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('230', '流水线运行列表', 'pipeline/ListRun', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('231', '获取流水线运行实例', 'pipeline/GetRun', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('232', '获取流水线日志', 'pipeline/GetJobLog', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('233', '通过AK获取组织ID', 'v1/GetOrganizationsByAk', '2', '215', '', '0', '', '2', '5');
INSERT INTO `node` VALUES ('234', '克隆', 'v1/Clone', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('235', '克隆', 'v1/Clone', '3', '39', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('236', '克隆', 'v1/Clone', '3', '50', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('237', '克隆', 'v1/Clone', '3', '52', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('238', '克隆', 'v1/Clone', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('239', '克隆', 'v1/Clone', '3', '53', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('240', 'beta1克隆', 'beta1/Clone', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('241', '副本', 'v1/Replicas', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('242', '升级策略', 'v1/Strategy', '3', '35', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('243', '运行一次', 'v1/Run', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('244', '运行一次', 'beta1/Run', '3', '42', '', '0', '', '2', '3');
INSERT INTO `node` VALUES ('245', 'websocket', 'terminal/ws', '3', '43', '', '0', '', '2', '3');

-- ----------------------------
-- Table structure for node_roles
-- ----------------------------
DROP TABLE IF EXISTS `node_roles`;
CREATE TABLE `node_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `node_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=872 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of node_roles
-- ----------------------------
INSERT INTO `node_roles` VALUES ('128', '32', '4');
INSERT INTO `node_roles` VALUES ('146', '1', '10');
INSERT INTO `node_roles` VALUES ('147', '2', '10');
INSERT INTO `node_roles` VALUES ('148', '3', '10');
INSERT INTO `node_roles` VALUES ('149', '4', '10');
INSERT INTO `node_roles` VALUES ('150', '5', '10');
INSERT INTO `node_roles` VALUES ('151', '27', '10');
INSERT INTO `node_roles` VALUES ('152', '28', '10');
INSERT INTO `node_roles` VALUES ('153', '6', '10');
INSERT INTO `node_roles` VALUES ('154', '7', '10');
INSERT INTO `node_roles` VALUES ('155', '8', '10');
INSERT INTO `node_roles` VALUES ('156', '9', '10');
INSERT INTO `node_roles` VALUES ('157', '10', '10');
INSERT INTO `node_roles` VALUES ('158', '11', '10');
INSERT INTO `node_roles` VALUES ('159', '12', '10');
INSERT INTO `node_roles` VALUES ('160', '13', '10');
INSERT INTO `node_roles` VALUES ('161', '14', '10');
INSERT INTO `node_roles` VALUES ('162', '15', '10');
INSERT INTO `node_roles` VALUES ('163', '16', '10');
INSERT INTO `node_roles` VALUES ('164', '17', '10');
INSERT INTO `node_roles` VALUES ('165', '18', '10');
INSERT INTO `node_roles` VALUES ('166', '19', '10');
INSERT INTO `node_roles` VALUES ('167', '20', '10');
INSERT INTO `node_roles` VALUES ('168', '21', '10');
INSERT INTO `node_roles` VALUES ('169', '22', '10');
INSERT INTO `node_roles` VALUES ('170', '23', '10');
INSERT INTO `node_roles` VALUES ('171', '24', '10');
INSERT INTO `node_roles` VALUES ('172', '29', '10');
INSERT INTO `node_roles` VALUES ('173', '30', '10');
INSERT INTO `node_roles` VALUES ('174', '31', '10');
INSERT INTO `node_roles` VALUES ('175', '72', '10');
INSERT INTO `node_roles` VALUES ('176', '73', '10');
INSERT INTO `node_roles` VALUES ('177', '74', '10');
INSERT INTO `node_roles` VALUES ('178', '75', '10');
INSERT INTO `node_roles` VALUES ('179', '76', '10');
INSERT INTO `node_roles` VALUES ('197', '34', '9');
INSERT INTO `node_roles` VALUES ('198', '35', '9');
INSERT INTO `node_roles` VALUES ('199', '85', '9');
INSERT INTO `node_roles` VALUES ('200', '37', '9');
INSERT INTO `node_roles` VALUES ('201', '80', '9');
INSERT INTO `node_roles` VALUES ('202', '39', '9');
INSERT INTO `node_roles` VALUES ('203', '97', '9');
INSERT INTO `node_roles` VALUES ('204', '40', '9');
INSERT INTO `node_roles` VALUES ('205', '105', '9');
INSERT INTO `node_roles` VALUES ('206', '41', '9');
INSERT INTO `node_roles` VALUES ('207', '112', '9');
INSERT INTO `node_roles` VALUES ('208', '42', '9');
INSERT INTO `node_roles` VALUES ('209', '120', '9');
INSERT INTO `node_roles` VALUES ('210', '43', '9');
INSERT INTO `node_roles` VALUES ('211', '151', '9');
INSERT INTO `node_roles` VALUES ('212', '44', '9');
INSERT INTO `node_roles` VALUES ('213', '179', '9');
INSERT INTO `node_roles` VALUES ('214', '45', '9');
INSERT INTO `node_roles` VALUES ('215', '156', '9');
INSERT INTO `node_roles` VALUES ('216', '48', '9');
INSERT INTO `node_roles` VALUES ('217', '171', '9');
INSERT INTO `node_roles` VALUES ('218', '49', '9');
INSERT INTO `node_roles` VALUES ('219', '166', '9');
INSERT INTO `node_roles` VALUES ('220', '167', '9');
INSERT INTO `node_roles` VALUES ('221', '50', '9');
INSERT INTO `node_roles` VALUES ('222', '127', '9');
INSERT INTO `node_roles` VALUES ('223', '51', '9');
INSERT INTO `node_roles` VALUES ('224', '145', '9');
INSERT INTO `node_roles` VALUES ('225', '52', '9');
INSERT INTO `node_roles` VALUES ('226', '133', '9');
INSERT INTO `node_roles` VALUES ('227', '53', '9');
INSERT INTO `node_roles` VALUES ('228', '140', '9');
INSERT INTO `node_roles` VALUES ('229', '65', '9');
INSERT INTO `node_roles` VALUES ('230', '69', '9');
INSERT INTO `node_roles` VALUES ('231', '34', '8');
INSERT INTO `node_roles` VALUES ('232', '35', '8');
INSERT INTO `node_roles` VALUES ('233', '82', '8');
INSERT INTO `node_roles` VALUES ('234', '37', '8');
INSERT INTO `node_roles` VALUES ('235', '77', '8');
INSERT INTO `node_roles` VALUES ('236', '39', '8');
INSERT INTO `node_roles` VALUES ('237', '95', '8');
INSERT INTO `node_roles` VALUES ('238', '40', '8');
INSERT INTO `node_roles` VALUES ('239', '103', '8');
INSERT INTO `node_roles` VALUES ('240', '41', '8');
INSERT INTO `node_roles` VALUES ('241', '110', '8');
INSERT INTO `node_roles` VALUES ('242', '42', '8');
INSERT INTO `node_roles` VALUES ('243', '117', '8');
INSERT INTO `node_roles` VALUES ('244', '45', '8');
INSERT INTO `node_roles` VALUES ('245', '155', '8');
INSERT INTO `node_roles` VALUES ('246', '47', '8');
INSERT INTO `node_roles` VALUES ('247', '175', '8');
INSERT INTO `node_roles` VALUES ('248', '176', '8');
INSERT INTO `node_roles` VALUES ('249', '48', '8');
INSERT INTO `node_roles` VALUES ('250', '170', '8');
INSERT INTO `node_roles` VALUES ('251', '50', '8');
INSERT INTO `node_roles` VALUES ('252', '125', '8');
INSERT INTO `node_roles` VALUES ('253', '51', '8');
INSERT INTO `node_roles` VALUES ('254', '143', '8');
INSERT INTO `node_roles` VALUES ('255', '52', '8');
INSERT INTO `node_roles` VALUES ('256', '131', '8');
INSERT INTO `node_roles` VALUES ('257', '53', '8');
INSERT INTO `node_roles` VALUES ('258', '138', '8');
INSERT INTO `node_roles` VALUES ('259', '65', '8');
INSERT INTO `node_roles` VALUES ('260', '67', '8');
INSERT INTO `node_roles` VALUES ('261', '34', '7');
INSERT INTO `node_roles` VALUES ('262', '35', '7');
INSERT INTO `node_roles` VALUES ('263', '83', '7');
INSERT INTO `node_roles` VALUES ('264', '84', '7');
INSERT INTO `node_roles` VALUES ('265', '37', '7');
INSERT INTO `node_roles` VALUES ('266', '78', '7');
INSERT INTO `node_roles` VALUES ('267', '79', '7');
INSERT INTO `node_roles` VALUES ('268', '39', '7');
INSERT INTO `node_roles` VALUES ('269', '96', '7');
INSERT INTO `node_roles` VALUES ('270', '40', '7');
INSERT INTO `node_roles` VALUES ('271', '104', '7');
INSERT INTO `node_roles` VALUES ('272', '107', '7');
INSERT INTO `node_roles` VALUES ('273', '41', '7');
INSERT INTO `node_roles` VALUES ('274', '111', '7');
INSERT INTO `node_roles` VALUES ('275', '42', '7');
INSERT INTO `node_roles` VALUES ('276', '118', '7');
INSERT INTO `node_roles` VALUES ('277', '119', '7');
INSERT INTO `node_roles` VALUES ('278', '48', '7');
INSERT INTO `node_roles` VALUES ('279', '172', '7');
INSERT INTO `node_roles` VALUES ('280', '50', '7');
INSERT INTO `node_roles` VALUES ('281', '126', '7');
INSERT INTO `node_roles` VALUES ('282', '144', '7');
INSERT INTO `node_roles` VALUES ('283', '52', '7');
INSERT INTO `node_roles` VALUES ('284', '132', '7');
INSERT INTO `node_roles` VALUES ('285', '53', '7');
INSERT INTO `node_roles` VALUES ('286', '139', '7');
INSERT INTO `node_roles` VALUES ('287', '65', '7');
INSERT INTO `node_roles` VALUES ('288', '68', '7');
INSERT INTO `node_roles` VALUES ('289', '34', '6');
INSERT INTO `node_roles` VALUES ('290', '35', '6');
INSERT INTO `node_roles` VALUES ('291', '36', '6');
INSERT INTO `node_roles` VALUES ('292', '81', '6');
INSERT INTO `node_roles` VALUES ('293', '86', '6');
INSERT INTO `node_roles` VALUES ('294', '87', '6');
INSERT INTO `node_roles` VALUES ('295', '88', '6');
INSERT INTO `node_roles` VALUES ('296', '90', '6');
INSERT INTO `node_roles` VALUES ('297', '91', '6');
INSERT INTO `node_roles` VALUES ('298', '92', '6');
INSERT INTO `node_roles` VALUES ('299', '37', '6');
INSERT INTO `node_roles` VALUES ('300', '38', '6');
INSERT INTO `node_roles` VALUES ('301', '80', '6');
INSERT INTO `node_roles` VALUES ('302', '201', '6');
INSERT INTO `node_roles` VALUES ('303', '39', '6');
INSERT INTO `node_roles` VALUES ('304', '93', '6');
INSERT INTO `node_roles` VALUES ('305', '94', '6');
INSERT INTO `node_roles` VALUES ('306', '98', '6');
INSERT INTO `node_roles` VALUES ('307', '99', '6');
INSERT INTO `node_roles` VALUES ('308', '100', '6');
INSERT INTO `node_roles` VALUES ('309', '40', '6');
INSERT INTO `node_roles` VALUES ('310', '101', '6');
INSERT INTO `node_roles` VALUES ('311', '102', '6');
INSERT INTO `node_roles` VALUES ('312', '106', '6');
INSERT INTO `node_roles` VALUES ('313', '41', '6');
INSERT INTO `node_roles` VALUES ('314', '108', '6');
INSERT INTO `node_roles` VALUES ('315', '109', '6');
INSERT INTO `node_roles` VALUES ('316', '113', '6');
INSERT INTO `node_roles` VALUES ('317', '114', '6');
INSERT INTO `node_roles` VALUES ('318', '42', '6');
INSERT INTO `node_roles` VALUES ('319', '115', '6');
INSERT INTO `node_roles` VALUES ('320', '116', '6');
INSERT INTO `node_roles` VALUES ('321', '121', '6');
INSERT INTO `node_roles` VALUES ('322', '122', '6');
INSERT INTO `node_roles` VALUES ('323', '43', '6');
INSERT INTO `node_roles` VALUES ('324', '147', '6');
INSERT INTO `node_roles` VALUES ('325', '148', '6');
INSERT INTO `node_roles` VALUES ('326', '149', '6');
INSERT INTO `node_roles` VALUES ('327', '150', '6');
INSERT INTO `node_roles` VALUES ('328', '152', '6');
INSERT INTO `node_roles` VALUES ('329', '44', '6');
INSERT INTO `node_roles` VALUES ('330', '177', '6');
INSERT INTO `node_roles` VALUES ('331', '178', '6');
INSERT INTO `node_roles` VALUES ('332', '45', '6');
INSERT INTO `node_roles` VALUES ('333', '153', '6');
INSERT INTO `node_roles` VALUES ('334', '154', '6');
INSERT INTO `node_roles` VALUES ('335', '46', '6');
INSERT INTO `node_roles` VALUES ('336', '180', '6');
INSERT INTO `node_roles` VALUES ('337', '48', '6');
INSERT INTO `node_roles` VALUES ('338', '168', '6');
INSERT INTO `node_roles` VALUES ('339', '169', '6');
INSERT INTO `node_roles` VALUES ('340', '173', '6');
INSERT INTO `node_roles` VALUES ('341', '174', '6');
INSERT INTO `node_roles` VALUES ('342', '49', '6');
INSERT INTO `node_roles` VALUES ('343', '161', '6');
INSERT INTO `node_roles` VALUES ('344', '162', '6');
INSERT INTO `node_roles` VALUES ('345', '163', '6');
INSERT INTO `node_roles` VALUES ('346', '164', '6');
INSERT INTO `node_roles` VALUES ('347', '50', '6');
INSERT INTO `node_roles` VALUES ('348', '123', '6');
INSERT INTO `node_roles` VALUES ('349', '124', '6');
INSERT INTO `node_roles` VALUES ('350', '128', '6');
INSERT INTO `node_roles` VALUES ('351', '51', '6');
INSERT INTO `node_roles` VALUES ('352', '141', '6');
INSERT INTO `node_roles` VALUES ('353', '142', '6');
INSERT INTO `node_roles` VALUES ('354', '146', '6');
INSERT INTO `node_roles` VALUES ('355', '52', '6');
INSERT INTO `node_roles` VALUES ('356', '129', '6');
INSERT INTO `node_roles` VALUES ('357', '130', '6');
INSERT INTO `node_roles` VALUES ('358', '134', '6');
INSERT INTO `node_roles` VALUES ('359', '53', '6');
INSERT INTO `node_roles` VALUES ('360', '135', '6');
INSERT INTO `node_roles` VALUES ('361', '136', '6');
INSERT INTO `node_roles` VALUES ('362', '137', '6');
INSERT INTO `node_roles` VALUES ('363', '54', '6');
INSERT INTO `node_roles` VALUES ('364', '188', '6');
INSERT INTO `node_roles` VALUES ('365', '189', '6');
INSERT INTO `node_roles` VALUES ('366', '190', '6');
INSERT INTO `node_roles` VALUES ('367', '55', '6');
INSERT INTO `node_roles` VALUES ('368', '185', '6');
INSERT INTO `node_roles` VALUES ('369', '186', '6');
INSERT INTO `node_roles` VALUES ('370', '187', '6');
INSERT INTO `node_roles` VALUES ('371', '56', '6');
INSERT INTO `node_roles` VALUES ('372', '182', '6');
INSERT INTO `node_roles` VALUES ('373', '183', '6');
INSERT INTO `node_roles` VALUES ('374', '184', '6');
INSERT INTO `node_roles` VALUES ('375', '57', '6');
INSERT INTO `node_roles` VALUES ('376', '191', '6');
INSERT INTO `node_roles` VALUES ('377', '196', '6');
INSERT INTO `node_roles` VALUES ('378', '58', '6');
INSERT INTO `node_roles` VALUES ('379', '192', '6');
INSERT INTO `node_roles` VALUES ('380', '197', '6');
INSERT INTO `node_roles` VALUES ('381', '59', '6');
INSERT INTO `node_roles` VALUES ('382', '193', '6');
INSERT INTO `node_roles` VALUES ('383', '198', '6');
INSERT INTO `node_roles` VALUES ('384', '60', '6');
INSERT INTO `node_roles` VALUES ('385', '194', '6');
INSERT INTO `node_roles` VALUES ('386', '199', '6');
INSERT INTO `node_roles` VALUES ('387', '61', '6');
INSERT INTO `node_roles` VALUES ('388', '195', '6');
INSERT INTO `node_roles` VALUES ('389', '200', '6');
INSERT INTO `node_roles` VALUES ('390', '62', '6');
INSERT INTO `node_roles` VALUES ('391', '157', '6');
INSERT INTO `node_roles` VALUES ('392', '158', '6');
INSERT INTO `node_roles` VALUES ('393', '159', '6');
INSERT INTO `node_roles` VALUES ('394', '160', '6');
INSERT INTO `node_roles` VALUES ('395', '181', '6');
INSERT INTO `node_roles` VALUES ('396', '65', '6');
INSERT INTO `node_roles` VALUES ('397', '66', '6');
INSERT INTO `node_roles` VALUES ('398', '70', '6');
INSERT INTO `node_roles` VALUES ('399', '71', '6');
INSERT INTO `node_roles` VALUES ('400', '34', '5');
INSERT INTO `node_roles` VALUES ('401', '35', '5');
INSERT INTO `node_roles` VALUES ('402', '36', '5');
INSERT INTO `node_roles` VALUES ('403', '81', '5');
INSERT INTO `node_roles` VALUES ('404', '82', '5');
INSERT INTO `node_roles` VALUES ('405', '83', '5');
INSERT INTO `node_roles` VALUES ('406', '84', '5');
INSERT INTO `node_roles` VALUES ('407', '86', '5');
INSERT INTO `node_roles` VALUES ('408', '87', '5');
INSERT INTO `node_roles` VALUES ('409', '88', '5');
INSERT INTO `node_roles` VALUES ('410', '89', '5');
INSERT INTO `node_roles` VALUES ('411', '90', '5');
INSERT INTO `node_roles` VALUES ('412', '91', '5');
INSERT INTO `node_roles` VALUES ('413', '92', '5');
INSERT INTO `node_roles` VALUES ('414', '37', '5');
INSERT INTO `node_roles` VALUES ('415', '38', '5');
INSERT INTO `node_roles` VALUES ('416', '201', '5');
INSERT INTO `node_roles` VALUES ('417', '39', '5');
INSERT INTO `node_roles` VALUES ('418', '93', '5');
INSERT INTO `node_roles` VALUES ('419', '94', '5');
INSERT INTO `node_roles` VALUES ('420', '95', '5');
INSERT INTO `node_roles` VALUES ('421', '96', '5');
INSERT INTO `node_roles` VALUES ('422', '98', '5');
INSERT INTO `node_roles` VALUES ('423', '99', '5');
INSERT INTO `node_roles` VALUES ('424', '100', '5');
INSERT INTO `node_roles` VALUES ('425', '40', '5');
INSERT INTO `node_roles` VALUES ('426', '101', '5');
INSERT INTO `node_roles` VALUES ('427', '102', '5');
INSERT INTO `node_roles` VALUES ('428', '103', '5');
INSERT INTO `node_roles` VALUES ('429', '104', '5');
INSERT INTO `node_roles` VALUES ('430', '106', '5');
INSERT INTO `node_roles` VALUES ('431', '107', '5');
INSERT INTO `node_roles` VALUES ('432', '41', '5');
INSERT INTO `node_roles` VALUES ('433', '108', '5');
INSERT INTO `node_roles` VALUES ('434', '109', '5');
INSERT INTO `node_roles` VALUES ('435', '110', '5');
INSERT INTO `node_roles` VALUES ('436', '111', '5');
INSERT INTO `node_roles` VALUES ('437', '113', '5');
INSERT INTO `node_roles` VALUES ('438', '114', '5');
INSERT INTO `node_roles` VALUES ('439', '42', '5');
INSERT INTO `node_roles` VALUES ('440', '115', '5');
INSERT INTO `node_roles` VALUES ('441', '116', '5');
INSERT INTO `node_roles` VALUES ('442', '117', '5');
INSERT INTO `node_roles` VALUES ('443', '118', '5');
INSERT INTO `node_roles` VALUES ('444', '119', '5');
INSERT INTO `node_roles` VALUES ('445', '121', '5');
INSERT INTO `node_roles` VALUES ('446', '122', '5');
INSERT INTO `node_roles` VALUES ('447', '43', '5');
INSERT INTO `node_roles` VALUES ('448', '147', '5');
INSERT INTO `node_roles` VALUES ('449', '148', '5');
INSERT INTO `node_roles` VALUES ('450', '149', '5');
INSERT INTO `node_roles` VALUES ('451', '150', '5');
INSERT INTO `node_roles` VALUES ('452', '152', '5');
INSERT INTO `node_roles` VALUES ('453', '44', '5');
INSERT INTO `node_roles` VALUES ('454', '177', '5');
INSERT INTO `node_roles` VALUES ('455', '178', '5');
INSERT INTO `node_roles` VALUES ('456', '45', '5');
INSERT INTO `node_roles` VALUES ('457', '153', '5');
INSERT INTO `node_roles` VALUES ('458', '154', '5');
INSERT INTO `node_roles` VALUES ('459', '155', '5');
INSERT INTO `node_roles` VALUES ('460', '46', '5');
INSERT INTO `node_roles` VALUES ('461', '180', '5');
INSERT INTO `node_roles` VALUES ('462', '47', '5');
INSERT INTO `node_roles` VALUES ('463', '175', '5');
INSERT INTO `node_roles` VALUES ('464', '176', '5');
INSERT INTO `node_roles` VALUES ('465', '48', '5');
INSERT INTO `node_roles` VALUES ('466', '168', '5');
INSERT INTO `node_roles` VALUES ('467', '169', '5');
INSERT INTO `node_roles` VALUES ('468', '170', '5');
INSERT INTO `node_roles` VALUES ('469', '172', '5');
INSERT INTO `node_roles` VALUES ('470', '173', '5');
INSERT INTO `node_roles` VALUES ('471', '174', '5');
INSERT INTO `node_roles` VALUES ('472', '49', '5');
INSERT INTO `node_roles` VALUES ('473', '161', '5');
INSERT INTO `node_roles` VALUES ('474', '162', '5');
INSERT INTO `node_roles` VALUES ('475', '163', '5');
INSERT INTO `node_roles` VALUES ('476', '164', '5');
INSERT INTO `node_roles` VALUES ('477', '50', '5');
INSERT INTO `node_roles` VALUES ('478', '123', '5');
INSERT INTO `node_roles` VALUES ('479', '124', '5');
INSERT INTO `node_roles` VALUES ('480', '125', '5');
INSERT INTO `node_roles` VALUES ('481', '126', '5');
INSERT INTO `node_roles` VALUES ('482', '128', '5');
INSERT INTO `node_roles` VALUES ('483', '51', '5');
INSERT INTO `node_roles` VALUES ('484', '141', '5');
INSERT INTO `node_roles` VALUES ('485', '142', '5');
INSERT INTO `node_roles` VALUES ('486', '143', '5');
INSERT INTO `node_roles` VALUES ('487', '144', '5');
INSERT INTO `node_roles` VALUES ('488', '146', '5');
INSERT INTO `node_roles` VALUES ('489', '52', '5');
INSERT INTO `node_roles` VALUES ('490', '129', '5');
INSERT INTO `node_roles` VALUES ('491', '130', '5');
INSERT INTO `node_roles` VALUES ('492', '131', '5');
INSERT INTO `node_roles` VALUES ('493', '132', '5');
INSERT INTO `node_roles` VALUES ('494', '134', '5');
INSERT INTO `node_roles` VALUES ('495', '53', '5');
INSERT INTO `node_roles` VALUES ('496', '135', '5');
INSERT INTO `node_roles` VALUES ('497', '136', '5');
INSERT INTO `node_roles` VALUES ('498', '137', '5');
INSERT INTO `node_roles` VALUES ('499', '138', '5');
INSERT INTO `node_roles` VALUES ('500', '139', '5');
INSERT INTO `node_roles` VALUES ('501', '54', '5');
INSERT INTO `node_roles` VALUES ('502', '188', '5');
INSERT INTO `node_roles` VALUES ('503', '189', '5');
INSERT INTO `node_roles` VALUES ('504', '190', '5');
INSERT INTO `node_roles` VALUES ('505', '55', '5');
INSERT INTO `node_roles` VALUES ('506', '185', '5');
INSERT INTO `node_roles` VALUES ('507', '186', '5');
INSERT INTO `node_roles` VALUES ('508', '187', '5');
INSERT INTO `node_roles` VALUES ('509', '56', '5');
INSERT INTO `node_roles` VALUES ('510', '182', '5');
INSERT INTO `node_roles` VALUES ('511', '183', '5');
INSERT INTO `node_roles` VALUES ('512', '184', '5');
INSERT INTO `node_roles` VALUES ('513', '57', '5');
INSERT INTO `node_roles` VALUES ('514', '191', '5');
INSERT INTO `node_roles` VALUES ('515', '196', '5');
INSERT INTO `node_roles` VALUES ('516', '58', '5');
INSERT INTO `node_roles` VALUES ('517', '192', '5');
INSERT INTO `node_roles` VALUES ('518', '197', '5');
INSERT INTO `node_roles` VALUES ('519', '59', '5');
INSERT INTO `node_roles` VALUES ('520', '193', '5');
INSERT INTO `node_roles` VALUES ('521', '198', '5');
INSERT INTO `node_roles` VALUES ('522', '60', '5');
INSERT INTO `node_roles` VALUES ('523', '194', '5');
INSERT INTO `node_roles` VALUES ('524', '199', '5');
INSERT INTO `node_roles` VALUES ('525', '61', '5');
INSERT INTO `node_roles` VALUES ('526', '195', '5');
INSERT INTO `node_roles` VALUES ('527', '200', '5');
INSERT INTO `node_roles` VALUES ('528', '62', '5');
INSERT INTO `node_roles` VALUES ('529', '157', '5');
INSERT INTO `node_roles` VALUES ('530', '158', '5');
INSERT INTO `node_roles` VALUES ('531', '159', '5');
INSERT INTO `node_roles` VALUES ('532', '160', '5');
INSERT INTO `node_roles` VALUES ('533', '181', '5');
INSERT INTO `node_roles` VALUES ('534', '65', '5');
INSERT INTO `node_roles` VALUES ('535', '66', '5');
INSERT INTO `node_roles` VALUES ('536', '67', '5');
INSERT INTO `node_roles` VALUES ('537', '68', '5');
INSERT INTO `node_roles` VALUES ('538', '70', '5');
INSERT INTO `node_roles` VALUES ('539', '71', '5');
INSERT INTO `node_roles` VALUES ('540', '1', '4');
INSERT INTO `node_roles` VALUES ('541', '2', '4');
INSERT INTO `node_roles` VALUES ('542', '3', '4');
INSERT INTO `node_roles` VALUES ('543', '4', '4');
INSERT INTO `node_roles` VALUES ('544', '5', '4');
INSERT INTO `node_roles` VALUES ('545', '27', '4');
INSERT INTO `node_roles` VALUES ('546', '28', '4');
INSERT INTO `node_roles` VALUES ('547', '6', '4');
INSERT INTO `node_roles` VALUES ('548', '7', '4');
INSERT INTO `node_roles` VALUES ('549', '8', '4');
INSERT INTO `node_roles` VALUES ('550', '9', '4');
INSERT INTO `node_roles` VALUES ('551', '10', '4');
INSERT INTO `node_roles` VALUES ('552', '11', '4');
INSERT INTO `node_roles` VALUES ('553', '12', '4');
INSERT INTO `node_roles` VALUES ('554', '13', '4');
INSERT INTO `node_roles` VALUES ('555', '14', '4');
INSERT INTO `node_roles` VALUES ('556', '15', '4');
INSERT INTO `node_roles` VALUES ('557', '16', '4');
INSERT INTO `node_roles` VALUES ('558', '17', '4');
INSERT INTO `node_roles` VALUES ('559', '18', '4');
INSERT INTO `node_roles` VALUES ('560', '19', '4');
INSERT INTO `node_roles` VALUES ('561', '20', '4');
INSERT INTO `node_roles` VALUES ('562', '21', '4');
INSERT INTO `node_roles` VALUES ('563', '22', '4');
INSERT INTO `node_roles` VALUES ('564', '23', '4');
INSERT INTO `node_roles` VALUES ('565', '24', '4');
INSERT INTO `node_roles` VALUES ('566', '29', '4');
INSERT INTO `node_roles` VALUES ('567', '30', '4');
INSERT INTO `node_roles` VALUES ('568', '31', '4');
INSERT INTO `node_roles` VALUES ('569', '72', '4');
INSERT INTO `node_roles` VALUES ('570', '73', '4');
INSERT INTO `node_roles` VALUES ('571', '74', '4');
INSERT INTO `node_roles` VALUES ('572', '75', '4');
INSERT INTO `node_roles` VALUES ('573', '76', '4');
INSERT INTO `node_roles` VALUES ('574', '34', '4');
INSERT INTO `node_roles` VALUES ('575', '35', '4');
INSERT INTO `node_roles` VALUES ('576', '36', '4');
INSERT INTO `node_roles` VALUES ('577', '81', '4');
INSERT INTO `node_roles` VALUES ('578', '82', '4');
INSERT INTO `node_roles` VALUES ('579', '83', '4');
INSERT INTO `node_roles` VALUES ('580', '84', '4');
INSERT INTO `node_roles` VALUES ('581', '85', '4');
INSERT INTO `node_roles` VALUES ('582', '86', '4');
INSERT INTO `node_roles` VALUES ('583', '87', '4');
INSERT INTO `node_roles` VALUES ('584', '88', '4');
INSERT INTO `node_roles` VALUES ('585', '89', '4');
INSERT INTO `node_roles` VALUES ('586', '90', '4');
INSERT INTO `node_roles` VALUES ('587', '91', '4');
INSERT INTO `node_roles` VALUES ('588', '92', '4');
INSERT INTO `node_roles` VALUES ('589', '37', '4');
INSERT INTO `node_roles` VALUES ('590', '38', '4');
INSERT INTO `node_roles` VALUES ('591', '77', '4');
INSERT INTO `node_roles` VALUES ('592', '78', '4');
INSERT INTO `node_roles` VALUES ('593', '79', '4');
INSERT INTO `node_roles` VALUES ('594', '80', '4');
INSERT INTO `node_roles` VALUES ('595', '201', '4');
INSERT INTO `node_roles` VALUES ('596', '39', '4');
INSERT INTO `node_roles` VALUES ('597', '93', '4');
INSERT INTO `node_roles` VALUES ('598', '94', '4');
INSERT INTO `node_roles` VALUES ('599', '95', '4');
INSERT INTO `node_roles` VALUES ('600', '96', '4');
INSERT INTO `node_roles` VALUES ('601', '97', '4');
INSERT INTO `node_roles` VALUES ('602', '98', '4');
INSERT INTO `node_roles` VALUES ('603', '99', '4');
INSERT INTO `node_roles` VALUES ('604', '100', '4');
INSERT INTO `node_roles` VALUES ('605', '40', '4');
INSERT INTO `node_roles` VALUES ('606', '101', '4');
INSERT INTO `node_roles` VALUES ('607', '102', '4');
INSERT INTO `node_roles` VALUES ('608', '103', '4');
INSERT INTO `node_roles` VALUES ('609', '104', '4');
INSERT INTO `node_roles` VALUES ('610', '105', '4');
INSERT INTO `node_roles` VALUES ('611', '106', '4');
INSERT INTO `node_roles` VALUES ('612', '107', '4');
INSERT INTO `node_roles` VALUES ('613', '41', '4');
INSERT INTO `node_roles` VALUES ('614', '108', '4');
INSERT INTO `node_roles` VALUES ('615', '109', '4');
INSERT INTO `node_roles` VALUES ('616', '110', '4');
INSERT INTO `node_roles` VALUES ('617', '111', '4');
INSERT INTO `node_roles` VALUES ('618', '112', '4');
INSERT INTO `node_roles` VALUES ('619', '113', '4');
INSERT INTO `node_roles` VALUES ('620', '114', '4');
INSERT INTO `node_roles` VALUES ('621', '42', '4');
INSERT INTO `node_roles` VALUES ('622', '115', '4');
INSERT INTO `node_roles` VALUES ('623', '116', '4');
INSERT INTO `node_roles` VALUES ('624', '117', '4');
INSERT INTO `node_roles` VALUES ('625', '118', '4');
INSERT INTO `node_roles` VALUES ('626', '119', '4');
INSERT INTO `node_roles` VALUES ('627', '120', '4');
INSERT INTO `node_roles` VALUES ('628', '121', '4');
INSERT INTO `node_roles` VALUES ('629', '122', '4');
INSERT INTO `node_roles` VALUES ('630', '43', '4');
INSERT INTO `node_roles` VALUES ('631', '147', '4');
INSERT INTO `node_roles` VALUES ('632', '148', '4');
INSERT INTO `node_roles` VALUES ('633', '149', '4');
INSERT INTO `node_roles` VALUES ('634', '150', '4');
INSERT INTO `node_roles` VALUES ('635', '151', '4');
INSERT INTO `node_roles` VALUES ('636', '152', '4');
INSERT INTO `node_roles` VALUES ('637', '44', '4');
INSERT INTO `node_roles` VALUES ('638', '177', '4');
INSERT INTO `node_roles` VALUES ('639', '178', '4');
INSERT INTO `node_roles` VALUES ('640', '179', '4');
INSERT INTO `node_roles` VALUES ('641', '45', '4');
INSERT INTO `node_roles` VALUES ('642', '153', '4');
INSERT INTO `node_roles` VALUES ('643', '154', '4');
INSERT INTO `node_roles` VALUES ('644', '155', '4');
INSERT INTO `node_roles` VALUES ('645', '156', '4');
INSERT INTO `node_roles` VALUES ('646', '46', '4');
INSERT INTO `node_roles` VALUES ('647', '180', '4');
INSERT INTO `node_roles` VALUES ('648', '47', '4');
INSERT INTO `node_roles` VALUES ('649', '175', '4');
INSERT INTO `node_roles` VALUES ('650', '176', '4');
INSERT INTO `node_roles` VALUES ('651', '48', '4');
INSERT INTO `node_roles` VALUES ('652', '168', '4');
INSERT INTO `node_roles` VALUES ('653', '169', '4');
INSERT INTO `node_roles` VALUES ('654', '170', '4');
INSERT INTO `node_roles` VALUES ('655', '171', '4');
INSERT INTO `node_roles` VALUES ('656', '172', '4');
INSERT INTO `node_roles` VALUES ('657', '173', '4');
INSERT INTO `node_roles` VALUES ('658', '174', '4');
INSERT INTO `node_roles` VALUES ('659', '49', '4');
INSERT INTO `node_roles` VALUES ('660', '161', '4');
INSERT INTO `node_roles` VALUES ('661', '162', '4');
INSERT INTO `node_roles` VALUES ('662', '163', '4');
INSERT INTO `node_roles` VALUES ('663', '164', '4');
INSERT INTO `node_roles` VALUES ('664', '165', '4');
INSERT INTO `node_roles` VALUES ('665', '166', '4');
INSERT INTO `node_roles` VALUES ('666', '167', '4');
INSERT INTO `node_roles` VALUES ('667', '50', '4');
INSERT INTO `node_roles` VALUES ('668', '123', '4');
INSERT INTO `node_roles` VALUES ('669', '124', '4');
INSERT INTO `node_roles` VALUES ('670', '125', '4');
INSERT INTO `node_roles` VALUES ('671', '126', '4');
INSERT INTO `node_roles` VALUES ('672', '127', '4');
INSERT INTO `node_roles` VALUES ('673', '128', '4');
INSERT INTO `node_roles` VALUES ('674', '51', '4');
INSERT INTO `node_roles` VALUES ('675', '141', '4');
INSERT INTO `node_roles` VALUES ('676', '142', '4');
INSERT INTO `node_roles` VALUES ('677', '143', '4');
INSERT INTO `node_roles` VALUES ('678', '144', '4');
INSERT INTO `node_roles` VALUES ('679', '145', '4');
INSERT INTO `node_roles` VALUES ('680', '146', '4');
INSERT INTO `node_roles` VALUES ('681', '52', '4');
INSERT INTO `node_roles` VALUES ('682', '129', '4');
INSERT INTO `node_roles` VALUES ('683', '130', '4');
INSERT INTO `node_roles` VALUES ('684', '131', '4');
INSERT INTO `node_roles` VALUES ('685', '132', '4');
INSERT INTO `node_roles` VALUES ('686', '133', '4');
INSERT INTO `node_roles` VALUES ('687', '134', '4');
INSERT INTO `node_roles` VALUES ('688', '53', '4');
INSERT INTO `node_roles` VALUES ('689', '135', '4');
INSERT INTO `node_roles` VALUES ('690', '136', '4');
INSERT INTO `node_roles` VALUES ('691', '137', '4');
INSERT INTO `node_roles` VALUES ('692', '138', '4');
INSERT INTO `node_roles` VALUES ('693', '139', '4');
INSERT INTO `node_roles` VALUES ('694', '140', '4');
INSERT INTO `node_roles` VALUES ('695', '54', '4');
INSERT INTO `node_roles` VALUES ('696', '188', '4');
INSERT INTO `node_roles` VALUES ('697', '189', '4');
INSERT INTO `node_roles` VALUES ('698', '190', '4');
INSERT INTO `node_roles` VALUES ('699', '55', '4');
INSERT INTO `node_roles` VALUES ('700', '185', '4');
INSERT INTO `node_roles` VALUES ('701', '186', '4');
INSERT INTO `node_roles` VALUES ('702', '187', '4');
INSERT INTO `node_roles` VALUES ('703', '56', '4');
INSERT INTO `node_roles` VALUES ('704', '182', '4');
INSERT INTO `node_roles` VALUES ('705', '183', '4');
INSERT INTO `node_roles` VALUES ('706', '184', '4');
INSERT INTO `node_roles` VALUES ('707', '57', '4');
INSERT INTO `node_roles` VALUES ('708', '191', '4');
INSERT INTO `node_roles` VALUES ('709', '196', '4');
INSERT INTO `node_roles` VALUES ('710', '58', '4');
INSERT INTO `node_roles` VALUES ('711', '192', '4');
INSERT INTO `node_roles` VALUES ('712', '197', '4');
INSERT INTO `node_roles` VALUES ('713', '59', '4');
INSERT INTO `node_roles` VALUES ('714', '193', '4');
INSERT INTO `node_roles` VALUES ('715', '198', '4');
INSERT INTO `node_roles` VALUES ('716', '60', '4');
INSERT INTO `node_roles` VALUES ('717', '194', '4');
INSERT INTO `node_roles` VALUES ('718', '199', '4');
INSERT INTO `node_roles` VALUES ('719', '61', '4');
INSERT INTO `node_roles` VALUES ('720', '195', '4');
INSERT INTO `node_roles` VALUES ('721', '200', '4');
INSERT INTO `node_roles` VALUES ('722', '62', '4');
INSERT INTO `node_roles` VALUES ('723', '157', '4');
INSERT INTO `node_roles` VALUES ('724', '158', '4');
INSERT INTO `node_roles` VALUES ('725', '159', '4');
INSERT INTO `node_roles` VALUES ('726', '160', '4');
INSERT INTO `node_roles` VALUES ('727', '181', '4');
INSERT INTO `node_roles` VALUES ('728', '65', '4');
INSERT INTO `node_roles` VALUES ('729', '66', '4');
INSERT INTO `node_roles` VALUES ('730', '67', '4');
INSERT INTO `node_roles` VALUES ('731', '68', '4');
INSERT INTO `node_roles` VALUES ('732', '69', '4');
INSERT INTO `node_roles` VALUES ('733', '70', '4');
INSERT INTO `node_roles` VALUES ('734', '71', '4');
INSERT INTO `node_roles` VALUES ('735', '207', '9');
INSERT INTO `node_roles` VALUES ('736', '204', '8');
INSERT INTO `node_roles` VALUES ('737', '205', '7');
INSERT INTO `node_roles` VALUES ('738', '206', '7');
INSERT INTO `node_roles` VALUES ('739', '202', '6');
INSERT INTO `node_roles` VALUES ('740', '203', '6');
INSERT INTO `node_roles` VALUES ('741', '208', '6');
INSERT INTO `node_roles` VALUES ('742', '209', '6');
INSERT INTO `node_roles` VALUES ('743', '202', '5');
INSERT INTO `node_roles` VALUES ('744', '203', '5');
INSERT INTO `node_roles` VALUES ('745', '204', '5');
INSERT INTO `node_roles` VALUES ('746', '205', '5');
INSERT INTO `node_roles` VALUES ('747', '206', '5');
INSERT INTO `node_roles` VALUES ('748', '207', '5');
INSERT INTO `node_roles` VALUES ('749', '208', '5');
INSERT INTO `node_roles` VALUES ('750', '209', '5');
INSERT INTO `node_roles` VALUES ('751', '202', '4');
INSERT INTO `node_roles` VALUES ('752', '203', '4');
INSERT INTO `node_roles` VALUES ('753', '204', '4');
INSERT INTO `node_roles` VALUES ('754', '205', '4');
INSERT INTO `node_roles` VALUES ('755', '206', '4');
INSERT INTO `node_roles` VALUES ('756', '207', '4');
INSERT INTO `node_roles` VALUES ('757', '208', '4');
INSERT INTO `node_roles` VALUES ('758', '209', '4');
INSERT INTO `node_roles` VALUES ('759', '181', '9');
INSERT INTO `node_roles` VALUES ('760', '214', '9');
INSERT INTO `node_roles` VALUES ('761', '215', '9');
INSERT INTO `node_roles` VALUES ('762', '220', '9');
INSERT INTO `node_roles` VALUES ('763', '223', '9');
INSERT INTO `node_roles` VALUES ('764', '181', '8');
INSERT INTO `node_roles` VALUES ('765', '211', '8');
INSERT INTO `node_roles` VALUES ('766', '215', '8');
INSERT INTO `node_roles` VALUES ('767', '217', '8');
INSERT INTO `node_roles` VALUES ('768', '222', '8');
INSERT INTO `node_roles` VALUES ('769', '181', '7');
INSERT INTO `node_roles` VALUES ('770', '212', '7');
INSERT INTO `node_roles` VALUES ('771', '213', '7');
INSERT INTO `node_roles` VALUES ('772', '215', '7');
INSERT INTO `node_roles` VALUES ('773', '218', '7');
INSERT INTO `node_roles` VALUES ('774', '219', '7');
INSERT INTO `node_roles` VALUES ('775', '227', '7');
INSERT INTO `node_roles` VALUES ('776', '210', '6');
INSERT INTO `node_roles` VALUES ('777', '215', '6');
INSERT INTO `node_roles` VALUES ('778', '216', '6');
INSERT INTO `node_roles` VALUES ('779', '221', '6');
INSERT INTO `node_roles` VALUES ('780', '224', '6');
INSERT INTO `node_roles` VALUES ('781', '225', '6');
INSERT INTO `node_roles` VALUES ('782', '226', '6');
INSERT INTO `node_roles` VALUES ('783', '227', '6');
INSERT INTO `node_roles` VALUES ('784', '228', '6');
INSERT INTO `node_roles` VALUES ('785', '229', '6');
INSERT INTO `node_roles` VALUES ('786', '230', '6');
INSERT INTO `node_roles` VALUES ('787', '231', '6');
INSERT INTO `node_roles` VALUES ('788', '232', '6');
INSERT INTO `node_roles` VALUES ('789', '210', '5');
INSERT INTO `node_roles` VALUES ('790', '211', '5');
INSERT INTO `node_roles` VALUES ('791', '212', '5');
INSERT INTO `node_roles` VALUES ('792', '213', '5');
INSERT INTO `node_roles` VALUES ('793', '215', '5');
INSERT INTO `node_roles` VALUES ('794', '216', '5');
INSERT INTO `node_roles` VALUES ('795', '217', '5');
INSERT INTO `node_roles` VALUES ('796', '218', '5');
INSERT INTO `node_roles` VALUES ('797', '219', '5');
INSERT INTO `node_roles` VALUES ('798', '221', '5');
INSERT INTO `node_roles` VALUES ('799', '222', '5');
INSERT INTO `node_roles` VALUES ('800', '224', '5');
INSERT INTO `node_roles` VALUES ('801', '225', '5');
INSERT INTO `node_roles` VALUES ('802', '226', '5');
INSERT INTO `node_roles` VALUES ('803', '227', '5');
INSERT INTO `node_roles` VALUES ('804', '228', '5');
INSERT INTO `node_roles` VALUES ('805', '229', '5');
INSERT INTO `node_roles` VALUES ('806', '230', '5');
INSERT INTO `node_roles` VALUES ('807', '231', '5');
INSERT INTO `node_roles` VALUES ('808', '232', '5');
INSERT INTO `node_roles` VALUES ('809', '210', '4');
INSERT INTO `node_roles` VALUES ('810', '211', '4');
INSERT INTO `node_roles` VALUES ('811', '212', '4');
INSERT INTO `node_roles` VALUES ('812', '213', '4');
INSERT INTO `node_roles` VALUES ('813', '214', '4');
INSERT INTO `node_roles` VALUES ('814', '215', '4');
INSERT INTO `node_roles` VALUES ('815', '216', '4');
INSERT INTO `node_roles` VALUES ('816', '217', '4');
INSERT INTO `node_roles` VALUES ('817', '218', '4');
INSERT INTO `node_roles` VALUES ('818', '219', '4');
INSERT INTO `node_roles` VALUES ('819', '220', '4');
INSERT INTO `node_roles` VALUES ('820', '221', '4');
INSERT INTO `node_roles` VALUES ('821', '222', '4');
INSERT INTO `node_roles` VALUES ('822', '223', '4');
INSERT INTO `node_roles` VALUES ('823', '224', '4');
INSERT INTO `node_roles` VALUES ('824', '225', '4');
INSERT INTO `node_roles` VALUES ('825', '226', '4');
INSERT INTO `node_roles` VALUES ('826', '227', '4');
INSERT INTO `node_roles` VALUES ('827', '228', '4');
INSERT INTO `node_roles` VALUES ('828', '229', '4');
INSERT INTO `node_roles` VALUES ('829', '230', '4');
INSERT INTO `node_roles` VALUES ('830', '231', '4');
INSERT INTO `node_roles` VALUES ('831', '232', '4');
INSERT INTO `node_roles` VALUES ('832', '233', '5');
INSERT INTO `node_roles` VALUES ('833', '233', '6');
INSERT INTO `node_roles` VALUES ('834', '233', '4');
INSERT INTO `node_roles` VALUES ('835', '222', '7');
INSERT INTO `node_roles` VALUES ('836', '234', '8');
INSERT INTO `node_roles` VALUES ('837', '235', '8');
INSERT INTO `node_roles` VALUES ('838', '238', '8');
INSERT INTO `node_roles` VALUES ('839', '240', '8');
INSERT INTO `node_roles` VALUES ('840', '236', '8');
INSERT INTO `node_roles` VALUES ('841', '237', '8');
INSERT INTO `node_roles` VALUES ('842', '239', '8');
INSERT INTO `node_roles` VALUES ('843', '234', '5');
INSERT INTO `node_roles` VALUES ('844', '235', '5');
INSERT INTO `node_roles` VALUES ('845', '238', '5');
INSERT INTO `node_roles` VALUES ('846', '240', '5');
INSERT INTO `node_roles` VALUES ('847', '236', '5');
INSERT INTO `node_roles` VALUES ('848', '237', '5');
INSERT INTO `node_roles` VALUES ('849', '239', '5');
INSERT INTO `node_roles` VALUES ('850', '234', '4');
INSERT INTO `node_roles` VALUES ('851', '235', '4');
INSERT INTO `node_roles` VALUES ('852', '238', '4');
INSERT INTO `node_roles` VALUES ('853', '240', '4');
INSERT INTO `node_roles` VALUES ('854', '236', '4');
INSERT INTO `node_roles` VALUES ('855', '237', '4');
INSERT INTO `node_roles` VALUES ('856', '239', '4');
INSERT INTO `node_roles` VALUES ('857', '241', '6');
INSERT INTO `node_roles` VALUES ('858', '242', '6');
INSERT INTO `node_roles` VALUES ('859', '243', '6');
INSERT INTO `node_roles` VALUES ('860', '244', '6');
INSERT INTO `node_roles` VALUES ('861', '241', '5');
INSERT INTO `node_roles` VALUES ('862', '242', '5');
INSERT INTO `node_roles` VALUES ('863', '243', '5');
INSERT INTO `node_roles` VALUES ('864', '244', '5');
INSERT INTO `node_roles` VALUES ('865', '241', '4');
INSERT INTO `node_roles` VALUES ('866', '242', '4');
INSERT INTO `node_roles` VALUES ('867', '243', '4');
INSERT INTO `node_roles` VALUES ('868', '244', '4');
INSERT INTO `node_roles` VALUES ('869', '245', '5');
INSERT INTO `node_roles` VALUES ('870', '245', '6');
INSERT INTO `node_roles` VALUES ('871', '245', '4');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL DEFAULT '',
  `name` varchar(100) NOT NULL DEFAULT '',
  `remark` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '2',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Admin role', '超级管理员', '超级管理员不受权限控制', '2');
INSERT INTO `role` VALUES ('4', '', '普通管理员', '具备全部权限', '2');
INSERT INTO `role` VALUES ('5', '', '普通用户', '具备普通的增改重启等权限', '2');
INSERT INTO `role` VALUES ('6', '', '只读角色', '只能读取资源信息', '2');
INSERT INTO `role` VALUES ('7', '', '修改角色', '修改资源信息', '2');
INSERT INTO `role` VALUES ('8', '', '添加角色', '新增资源信息', '2');
INSERT INTO `role` VALUES ('9', '', '删除角色', '可以删除相关资源', '2');
INSERT INTO `role` VALUES ('10', '', '权限角色', '权限管理', '2');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `sessionid` varchar(32) DEFAULT '',
  `username` varchar(32) NOT NULL DEFAULT '',
  `password` varchar(32) NOT NULL DEFAULT '',
  `nickname` varchar(32) NOT NULL DEFAULT '',
  `telphone` varchar(12) DEFAULT '',
  `company` varchar(255) DEFAULT '',
  `department` varchar(255) DEFAULT '',
  `email` varchar(32) NOT NULL DEFAULT '',
  `remark` varchar(200) DEFAULT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `lastloginip` varchar(255) DEFAULT '',
  `lastlogintime` varchar(255) DEFAULT '',
  `createtime` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '', 'admin', '21232f297a57a5a743894a0e4a801fc3', '超级管理员', '10001', '广东省xxx公司', '研发中心', 'admin@qq.com', '超级管理员', '1', '127.0.0.1:9817', '2024-04-03T17:14:49', '2015-07-24 10:06:49');

-- ----------------------------
-- Table structure for user_cluster
-- ----------------------------
DROP TABLE IF EXISTS `user_cluster`;
CREATE TABLE `user_cluster` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `cluster_id` varchar(20) NOT NULL,
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqStr` (`username`,`cluster_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_cluster
-- ----------------------------

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS `user_roles`;
CREATE TABLE `user_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user_roles
-- ----------------------------
INSERT INTO `user_roles` VALUES ('63', '3', '4');
INSERT INTO `user_roles` VALUES ('64', '5', '5');
INSERT INTO `user_roles` VALUES ('65', '2', '6');

-- ----------------------------
-- Table structure for wiki
-- ----------------------------
DROP TABLE IF EXISTS `wiki`;
CREATE TABLE `wiki` (
  `id` int(8) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL DEFAULT '',
  `xcolumn` varchar(64) DEFAULT 'default' COMMENT '文章分组',
  `sketch` varchar(255) DEFAULT '',
  `content` mediumtext,
  `author` varchar(64) DEFAULT '',
  `status` int(2) DEFAULT '1',
  `authkey` varchar(255) DEFAULT '',
  `createtime` varchar(32) NOT NULL DEFAULT '',
  `updatetime` varchar(32) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of wiki
-- ----------------------------
INSERT INTO `wiki` VALUES ('1', '安装说明文档', '帮助中心', '', '### 功能特性：\n------------\n>   该软件是一款基于client-go、layui、layuimini、beego开发的kubernetes多集群管理系统，该系统具备将多个IDC、公有云的k8s进行统一进行管理。比kubernetes dashboard的功能更丰富，界面更友好更直观。\n\n#### 具备功能\n                \n- 跨公有云、跨IDC的多k8s集群统一管理平台\n- 具备节点、节电池、命名空间、clusterRoleBinding、clusterRoles、RoleBinding、Roles、serviceAccounts的创建、详情、yaml文件查看、删除等功能。\n- workload方面支持对deployment、statefuleset、dameset、cronjob、job、pod容器组、cdr自定义资源、hpa伸缩的功能创建、yaml查看修改、删除功能。\n- 无状态【deployment]:功能具备yaml在线编辑、yaml下载,在线修改升级策略,在线标签修改,在线重启,镜像更新，查看关联的pod对象、查看关联的service、ingress，创建hpa对deployment进行自动伸缩容，可以在线操作回滚到指定的镜像版本，查看该deployment的相关事件，在线查看日志，ssh终端登录关联的pod。\n- 有状态【statefulset】：功能和deployment的类似，除有少量差异之外，相关功能基本一致。\n- 守护进程【daemonset】的功能主要是对deamonset的相关信息进行分类查看。\n- 任务【job】：具备日志、信息、事件、状态的查询功能。\n- 定时任务【cronjob】：在线通过图形库界面进行创建、yaml文件进行创建、对计划任务的在线修改，在线更改状态等功能。\n- 容器组【pod】：具备ssh登录、日志查看、实时查看pod的内存、cpu使用情况【需k8s环境安装metric-beat】等功能。\n- 扩缩容【hpa】：在线图形化操作，根据pod的cpu、内存使用情况、定义pod的扩缩容。\n- 自定义资源【cdr】:自定义资源的信息查看\n- yaml操作：可以通过在线的各种deployment、service、ingress、cronjob等yaml文件模板来进行资源的创建。\n- 服务【service】:支持通过yaml来进行创建，对service的yaml配置查看和修改、支持图形操作创建。\n- 路由【ingres】：支持通过yaml来进行创建，对service的yaml配置查看和修改、支持图形操作创建，目前只支持nginx-ingress。\n- 配置【configmap】：支持图形化、yaml配置的查看、创建、修改和删除。\n- 保密字典【secret】:支持图形化、yaml配置的查看、创建、修改和删除。\n- 存储声明【pvc】:pvc的yaml查看、创建、修改、删除。\n- 存储卷【pv】:存储卷信息的yaml查看、创建、修改、删除。\n- 存储类【storageclass】:存储类信息的yaml查看、创建、修改、删除。\n- 事件信息：查看当前集群中发生的事件信息。\n- 应用集：按照资源的标签appname=myapp进行划分，将该项目所涉及的资源整合到统一界面便于管理。\n- 权限管理：按照角色进行权限划分：超级管理员、普通管理员、只读等角色，并按照集群进行授权，只有授权的了对应集群权限的用户才能访问该集群的资源。\n- 文档中心：markdown格式的文档编辑器、用于运维文档记录。\n- 阿里云流水线：将阿里云流水线集成进来，通过在xkube操作阿里云流水线的运行、停止、查看日志、人工卡点等功能\n- 新增aws的eks管理：可以通过额外创建token，将aws的eks纳入管理\n     \n### 功能截图\n---\n\n\n#### 下一阶段计划\n---\n* 新增CICD持续集成、持续部署的功能。\n* 根据用户反馈新增功能、或修复bug。\n* 增加镜像、应用的管理。\n\n### 安装说明：\n---\n#### 第一步：安装mysql\n	安装完mysql以后，将xkube.sql导入到数据库中，然后修改conf/app.conf中的如下配置：\n	db_host = 127.0.0.1  #mysql的IP\n	db_port = 3306	#MySQL的端口\n	db_user = root	#mysql的用户名\n	db_pass = root#123	#mysqld 密码\n	db_name = db_xkube	#数据库名\n	\n#### 第二步：安装redis\n	安装完redis以后：然后修改conf/app.conf中的如下配置：\n	redisDb = \"192.168.1.17:6379\"	#redis的IP和端口\n	redisPasswd = \"redis#123\"	#redis配置设置的密码\n	SessionProviderConfig = \"192.168.1.17:6379,100,redis#123\"	#将redis的IP、端口、密码进行更改，100这个数字保留即可。\n	\n#### 第三步：启动服务\n	修改完以上配置后：进入xkube解压的目录直接执行：nohub ./xkube & \n	就可以通过http://ip:8001/index 进行访问了。注意:直接访问http://ip:8001/ 会出现404，需要附上/index 这个路径。\n#### 第四步：如果是在xkube前面配置nginx反向代理需要配置websocket\n```\n        location ~^/xkube/pod/terminal/ws {\n            proxy_pass http://127.0.0.1:8080;\n            proxy_http_version 1.1;\n            proxy_set_header Upgrade $http_upgrade;\n            proxy_set_header Connection \"upgrade\";\n            proxy_set_header Origin \"\";\n        }\n```\n', 'admin', '0', '', '2024-01-15 14:24:00', '2024-03-18 14:05:31');
INSERT INTO `wiki` VALUES ('2', 'test123', 'default', '', '- asdf\n>aaa', 'admin', '0', '827ccb0eea8a706c4c34a16891f84e7b', '2024-01-16 10:29:55', '2024-01-16 10:29:55');
INSERT INTO `wiki` VALUES ('3', '快速上手', '帮助中心', '', '###快速上手\n- 1.在安装搭建好后，通过用户名和密码admin登录后台\n- 2.第一步：添加集群kubeconfig配置、k8s管理-集群列表-添加\n- 3.第二步：添加多个集群后，可以在集群列表的后边设置经常使用的k8s为常用集群。这样每次登录后默认均为管理该k8s。如果多个k8s都经常使用，可以在工作负载资源列表或k8s列表进行切换。\n- 4.第三步：完成以上三步均可以对k8s进行管理使用了。\n- 5.运维管理-应用集管理：该功能旨在将多个资源进行分组，整合在统一的一个分组中。通过标签中的appname来实现。\n- 6.运维管理-CICD：该功能主要是对接阿里云的流水线来进行管理，首先添加阿里云的帐号及流水线组织ID，然后在流水线列表列添加上流水线的ID即可进行管理。', 'admin', '0', '', '2024-03-18 14:15:34', '2024-03-18 14:15:34');

-- ----------------------------
-- Table structure for xkb_appname
-- ----------------------------
DROP TABLE IF EXISTS `xkb_appname`;
CREATE TABLE `xkb_appname` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `appname` varchar(64) NOT NULL DEFAULT '' COMMENT '应用名',
  `remarks` varchar(255) DEFAULT '',
  `createtime` datetime DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqapp` (`appname`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xkb_appname
-- ----------------------------
INSERT INTO `xkb_appname` VALUES ('14', 'test-app', '测试应用', '2023-05-10 10:21:41');

-- ----------------------------
-- Table structure for xkb_cicd
-- ----------------------------
DROP TABLE IF EXISTS `xkb_cicd`;
CREATE TABLE `xkb_cicd` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cicd_name` varchar(255) NOT NULL DEFAULT '',
  `appname` varchar(255) DEFAULT '',
  `cluster_id` varchar(64) DEFAULT '',
  `namespace` varchar(64) DEFAULT '',
  `cicd_type` int(2) NOT NULL DEFAULT '0',
  `status` int(2) DEFAULT '1',
  `remarks` varchar(255) DEFAULT '',
  `last_runtime` varchar(32) DEFAULT '',
  `createtime` varchar(64) DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `uniqCicdName` (`cicd_name`,`cluster_id`,`namespace`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of xkb_cicd
-- ----------------------------

-- ----------------------------
-- Table structure for xkb_cicd_ak
-- ----------------------------
DROP TABLE IF EXISTS `xkb_cicd_ak`;
CREATE TABLE `xkb_cicd_ak` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `aliyun_id` varchar(255) NOT NULL DEFAULT '',
  `accesskey_id` varchar(255) NOT NULL DEFAULT '',
  `accesskey_secret` varchar(128) NOT NULL DEFAULT '',
  `organization_id` varchar(512) NOT NULL DEFAULT '',
  `remarks` varchar(255) DEFAULT '',
  `createtime` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of xkb_cicd_ak
-- ----------------------------

-- ----------------------------
-- Table structure for xkb_cicd_pipelines
-- ----------------------------
DROP TABLE IF EXISTS `xkb_cicd_pipelines`;
CREATE TABLE `xkb_cicd_pipelines` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cicd_id` int(11) NOT NULL DEFAULT '0',
  `aliyun_id` varchar(256) NOT NULL DEFAULT '',
  `organization_id` text NOT NULL,
  `pipeline_id` varchar(256) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of xkb_cicd_pipelines
-- ----------------------------

-- ----------------------------
-- Table structure for xkb_cluster
-- ----------------------------
DROP TABLE IF EXISTS `xkb_cluster`;
CREATE TABLE `xkb_cluster` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cluster_id` varchar(64) DEFAULT '',
  `cluster_name` varchar(64) DEFAULT '',
  `idc_name` varchar(64) DEFAULT '',
  `kube_version` varchar(64) DEFAULT '',
  `kube_config` text,
  `bearer_token` text COMMENT '用于二次认证例如aws的eks',
  `lan_slbip` varchar(64) DEFAULT '',
  `wan_slbip` varchar(64) DEFAULT '',
  `status` int(11) DEFAULT '1',
  `remarks` varchar(255) DEFAULT '',
  `createtime` varchar(32) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xkb_cluster
-- ----------------------------
